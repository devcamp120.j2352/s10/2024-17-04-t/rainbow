package com.devcamp.rainbow.services;

import java.util.ArrayList;

import org.springframework.stereotype.Service;

@Service
public class RainbowService {
    String[] rainbowList= {"red","orange","yellow","green","blue","indigo","violet"};

    public ArrayList<String> filterRainbowList(String filter) {
        ArrayList<String> filteredRainbows = new ArrayList<>();

        for (String rainbow: this.rainbowList) {
            // "orange"
            if (rainbow.contains(filter)) {
                filteredRainbows.add(rainbow);
            }
        }

        return filteredRainbows;
    }

    public String getRainbowByIndex(Integer index) {
        if (index<=-1 || index > 6) {
            return "";
        }

        return this.rainbowList[index];
    }
}
