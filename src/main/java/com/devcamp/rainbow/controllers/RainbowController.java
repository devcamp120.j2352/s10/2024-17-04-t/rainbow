package com.devcamp.rainbow.controllers;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.rainbow.services.RainbowService;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;


@RestController
@CrossOrigin
public class RainbowController {

    @Autowired
    RainbowService rainbowService;

    @GetMapping("/rainbow-request-query")
    public ArrayList<String> filterRainbowList(@RequestParam(value = "filter", required = true) String vFilter) {
        return rainbowService.filterRainbowList(vFilter);
    }

    @GetMapping("/rainbow-request-param/{i}")
    public String getRainbowByIndex(@PathVariable(name="i") Integer index) {
        return rainbowService.getRainbowByIndex(index);
    }
}
